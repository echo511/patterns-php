<?php


namespace App;


class PortCompilerExtension extends \Nette\DI\CompilerExtension
{

    public function __construct()
    {
        $this->config = new class {

            /**
             * @var string
             */
            public $name = 'unknown';

            public function getPosfixName(): string
            {
                return $this->name . 'pos';
            }

        };
    }


    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();

        $builder->addDefinition(null)
            ->setFactory(\Port::class)
            ->setArgument('name', $this->config->getPosfixName());

        $builder->addFactoryDefinition(null)
            ->setImplement(\ShipFactoryInterface::class)
            ->getResultDefinition()
            ->setFactory(\Ship::class);

        $builder->addDefinition(null)
            ->setFactory(\NotifyWhenShipHandled::class);

        $builder->addDefinition(null)
            ->setFactory(\Symfony\Component\EventDispatcher\EventDispatcher::class);
    }


    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();

        /** @var \Nette\DI\Definitions\FactoryDefinition $service */
        $service = $builder->getDefinitionByType(\ShipFactoryInterface::class);
        $service->getResultDefinition()->setFactory(\BetaShip::class);

        /** @var \Nette\DI\Definitions\ServiceDefinition $dispatcher */
        $dispatcher = $builder->getDefinitionByType(\Symfony\Component\EventDispatcher\EventDispatcher::class);
        foreach ($builder->findByType(\Symfony\Component\EventDispatcher\EventSubscriberInterface::class) as $subscriberDefinition) {
            $dispatcher->addSetup('addSubscriber', [$subscriberDefinition]);
        }
    }

}
