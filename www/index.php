<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

class Captain
{

    public $name;

}

interface ShipInterface
{

}

class ShipContainer implements PriceCalculableInterface
{

    public function calculate(Calculator $calculator = null): int
    {
        return 100;
    }

}

class CaptainsLogEntry
{

    private DateTimeImmutable $created;

    private bool $isDocked;


    public function __construct(bool $isDocked)
    {
        $this->created = new DateTimeImmutable();
        $this->isDocked = $isDocked;
    }


    /**
     * @return \DateTimeImmutable
     */
    public function getCreated(): \DateTimeImmutable
    {
        return $this->created;
    }


    /**
     * @return bool
     */
    public function isDocked(): bool
    {
        return $this->isDocked;
    }


}

class CaptainsLog
{

    /**
     * @var callable[]
     */
    public $onAdd = [];

    /**
     * @var \CaptainsLogEntry[]
     */
    private $logs = [];


    public function add(CaptainsLogEntry $entry)
    {
        $this->logs[] = $entry;
        foreach ($this->onAdd as $callable) {
            call_user_func($callable, $entry);
        }
    }

}

class Secretary
{

    public function ring(CaptainsLogEntry $entry)
    {
        echo $entry->getCreated()->format(DATE_ATOM);
        echo " Ring\n";
    }

}

interface Calculator
{

    public function modifyCalculation(int $price): int;

}

class NaiveCalculator implements Calculator
{

    private Ship $ship;


    public function modifyCalculation(int $price): int
    {
        return $price;
    }

}

class MultiplicatingCalculator implements Calculator
{

    private Ship $ship;


    public function modifyCalculation(int $price): int
    {
        return $price * 100;
    }

}

class Ship implements ShipInterface, PriceCalculableInterface
{

    public $name;

    public Captain $captain;

    public Secretary $secretary;

    private $isDocked = false;

    /**
     * @var ShipContainer[]
     */
    public $containers = [];

    /**
     * @var \CaptainsLog
     */
    private $log;

    private Calculator $calculator;


    /**
     * Ship constructor.
     *
     * @param $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->log = new CaptainsLog();
        $this->secretary = new Secretary();
        $this->log->onAdd[] = [$this->secretary, 'ring'];
        $this->calculator = new NaiveCalculator();
    }


    public function accept(Inspector $inspector)
    {
        $inspector->receivesShipPrice($this->calculate());
    }


    public function calculate(Calculator $calculator = null): int
    {
        if (!$calculator) {
            $calculator = $this->calculator;
        }

        $containersPrice = 0;
        foreach ($this->containers as $container) {
            $containersPrice += $container->calculate();
        }

        $price = 10000 + $containersPrice;

        return $calculator->modifyCalculation($price);
    }


    public function isDocked(): bool
    {
        return $this->isDocked;
    }


    public function dock(Port $port)
    {
        $this->isDocked = true;
        $this->log->add(
            $this->save()
        );
    }


    public function ship()
    {
        $this->isDocked = false;
        $this->log->add(
            $this->save()
        );
    }


    public function save(): CaptainsLogEntry
    {
        return new CaptainsLogEntry($this->isDocked);
    }


    public function restore(CaptainsLogEntry $captainsLogEntry): self
    {
        $ship = new self($this->name);
        $ship->isDocked = $captainsLogEntry->isDocked();

        return $ship;
    }

}

class CachedShipPrice implements PriceCalculableInterface
{

    private ?int $cached = null;

    private Ship $ship;


    public function __construct(\Ship $ship)
    {
        $this->ship = $ship;
    }


    public function calculate(Calculator $calculator = null): int
    {
        if (!$this->cached) {
            echo 'calculate';
            $this->cached = $this->ship->calculate($calculator);
        }

        return $this->cached;
    }

}

class BetaShip extends Ship
{

    public $s = ';';

}

interface ShipFactoryInterface
{

    public function create(string $name): ShipInterface;

}

interface SimpleCache
{

    public function save($data);


    public function get();

}

interface NetteCache
{

    public function put($data);

}

interface PriceCalculableInterface
{

    public function calculate(Calculator $calculator = null): int;

}

$simple = new class implements SimpleCache {

    private $cached = null;


    public function save($data)
    {
        $this->cached = $data;
    }


    public function get()
    {
        return $this->cached;
    }

};

$nette = new class implements NetteCache {

    private $cached = null;


    public function put($data)
    {
        $this->cached = $data;
    }


    public function get()
    {
        return $this->cached;
    }

};

$bridge = new class implements NetteCache {

    private SimpleCache $inner;


    public function __construct()
    {
        $this->inner = new class implements SimpleCache {

            private $cached = null;


            public function save($data)
            {
                $this->cached = $data;
            }


            public function get()
            {
                return $this->cached;
            }

        };
    }


    public function put($data)
    {
        $this->inner->save($data);
    }


    public function get()
    {
        return $this->inner->get();
    }

};


interface PortHandler
{


    /**
     * @param \Ship $ship
     * @return mixed
     * @throws \CannotHandleException
     */
    public function handle(Ship $ship);

}

class CannotHandleException extends RuntimeException
{

}

class DockingHandler implements PortHandler
{

    private Port $port;


    public function __construct(\Port $port)
    {
        $this->port = $port;
    }


    public function handle(Ship $ship)
    {
        if ($ship->isDocked()) {
            throw new CannotHandleException();
        }
        $ship->dock($this->port);
        echo "Docked\n";
    }

}

class ContainerHandler implements PortHandler
{

    private Port $port;


    public function __construct(\Port $port)
    {
        $this->port = $port;
    }


    public function handle(Ship $ship)
    {
        $ship->ship();
        echo "Shipped\n";
    }

}


class Port
{

    public const EVENT_SHIP_HANDLED = 'port.ship.handled';

    /**
     * @var \PortHandler[]
     */
    private $handlers = [];

    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    private $eventDispatcher;


    public function __construct(string $name, \Symfony\Component\EventDispatcher\EventDispatcher $eventDispatcher)
    {
        echo $name . "\n";
        $this->eventDispatcher = $eventDispatcher;
        $this->handlers = [
            new DockingHandler($this),
            new ContainerHandler($this),
        ];
    }


    public function handleShip(Ship $ship): void
    {
        foreach ($this->handlers as $handler) {
            try {
                $handler->handle($ship);
            } catch (CannotHandleException $exception) {
                continue;
            }
        }
        $event = new NotifyWhenShipHandledEvent($ship);
        $this->eventDispatcher->dispatch($event, self::EVENT_SHIP_HANDLED);
    }

}

interface Command
{

}

class DockShipCommand implements Command
{

    public Ship $ship;

    public Port $port;

}

interface CommandHandler
{

    public function handle(Command $command): bool;

}

class DockShipCommandHandler implements CommandHandler
{

    public function handle(Command $command): bool
    {
        if ($command instanceof DockShipCommand) {
            $command->port->handleShip($command->ship);

            return true;
        }

        return false;
    }

}

class ShipShipCommand implements Command
{

    public Ship $ship;

    public Port $port;

}

abstract class AbstractCOmmandHandler implements CommandHandler
{

    abstract protected function shouldActOnClass(): string;


    public function handle(Command $command): bool
    {
        $className = $this->shouldActOnClass();
        if ($command instanceof $className) {
            $command->port->handleShip($command->ship);

            return true;
        }

        return false;
    }

}

class ShipShipCommandHandler extends AbstractCOmmandHandler
{

    protected function shouldActOnClass(): string
    {
        return ShipShipCommand::class;
    }

}

class CommandBUS implements CommandHandler
{

    /**
     * @var \CommandHandler[]
     */
    private $commandHandlers = [];

    /**
     * @var \CommandHandler[]
     */
    private $executed = [];


    public function __construct()
    {
        $this->commandHandlers[] = new DockShipCommandHandler();
    }


    public function handle(Command $command): bool
    {
        foreach ($this->commandHandlers as $handler) {
            if ($handler->handle($command)) {
                $this->executed[] = $command;
            }
        }

        return true;
    }


    public function getExecuted(): iterable
    {
        if (is_array($this->executed)) {
            return $this->executed;
        }
    }

}

class ShipGenerator implements IteratorAggregate
{


    public function getIterator()
    {
        return new class implements Iterator {

            private $howMany = 10;

            private $current = 1;


            public function current()
            {
                return new Ship(\Nette\Utils\Random::generate());
            }


            public function next()
            {
                $this->current++;
            }


            public function key()
            {
                return $this->current;
            }


            public function valid()
            {
                return $this->current <= $this->howMany;
            }


            public function rewind()
            {
                $this->current = 1;
            }


        };
    }

}

class NotifyWhenShipHandledEvent
{

    private Ship $ship;


    public function __construct(\Ship $ship)
    {
        $this->ship = $ship;
    }


    /**
     * @return \Ship
     */
    public function getShip(): \Ship
    {
        return $this->ship;
    }


}

class NotifyWhenShipHandled implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            Port::EVENT_SHIP_HANDLED => 'notify',
        ];
    }


    public function notify(NotifyWhenShipHandledEvent $event)
    {
        echo "Ship price evented: " . $event->getShip()->calculate() . "\n";
    }

}

class Inspector
{

    private int $shipPrice;


    public function visits(Ship $ship)
    {
        $ship->accept($this);
    }


    public function receivesShipPrice(int $price)
    {
        $this->shipPrice = $price;
    }


    public function isOk(): bool
    {
        if (!$this->shipPrice) {
            throw new RuntimeException("Price not received.");
        }

        return $this->shipPrice < 10000;
    }

}

$configurator = App\Bootstrap::boot();
$configurator->setDebugMode(true);
$container = $configurator->createContainer();
//$shipFactory = $container->getByType(ShipFactoryInterface::class);
//
//var_dump($shipFactory->create('Hello'));

$app = new \Symfony\Component\Console\Application();

$app->add(new class extends \Symfony\Component\Console\Command\Command {

    public function __construct()
    {
        parent::__construct();
    }


    protected function configure()
    {
        $this->setName('neco');
        $this->setDescription('Neco se provede.');

        $this->addArgument('arg', \Symfony\Component\Console\Input\InputArgument::REQUIRED);
        $this->addOption('opt', 'o', \Symfony\Component\Console\Input\InputOption::VALUE_REQUIRED);
    }


    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
        $val = $input->getArgument('arg');
        $opt = $input->getOption('opt');

        $output->writeln('<info>WRONG</info>' . ' ' . $val);
        $output->writeln('option: ' . $opt);

        return self::SUCCESS;
    }

});

$app->run(new \Symfony\Component\Console\Input\ArgvInput(), new \Symfony\Component\Console\Output\ConsoleOutput());
exit;

$eventDispatcher = $container->getByType(\Symfony\Component\EventDispatcher\EventDispatcher::class);

$ship = new Ship('Roventa');
$ship->containers[] = new ShipContainer();
$ship->containers[] = new ShipContainer();

$port = $container->getByType(Port::class);
//$ship->dock($port);

$bus = new CommandBUS();

$dockShipCommand = new DockShipCommand();
$dockShipCommand->port = $port;
$dockShipCommand->ship = $ship;
$bus->handle($dockShipCommand);

var_dump($ship->calculate(new MultiplicatingCalculator()));

$inspector = new Inspector();
$inspector->visits($ship);
var_dump($inspector->isOk());
exit;

var_dump($bus);
var_dump($ship);
